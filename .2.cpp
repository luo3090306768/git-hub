#include<stdio.h>   
struct student 
{
    char id[6]; 
    char name[11]; 
    float grade;   
};
 
int main()
{
    struct student stu[10]; 
    int i, N;
    float sum = 0, average;
    scanf("%d\n", &N);
    for (i = 0; i < N; i++) 
    {
        scanf("%s%s%f", &stu[i].id, &stu[i].name, &stu[i].grade);
        sum += stu[i].grade;
    }
    average = sum / N;
 
    printf("%.2f\n", average);
    for (i = 0; i<N; i++)  
    {
        if (stu[i].grade< average)
        {
            printf("%s %s\n", stu[i].name, stu[i].id);
        }
    }
 
    return 0;
}

